import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;

public class BookManager {

    public static void main(String[] args)

    {

        // Loads books from csv

        try {
            String csvFile = "data/books.csv";

            FileReader fileReader = new FileReader(csvFile);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            Book[] books = new Book[10];

            String nextLine = bufferedReader.readLine();

            for (int i = 0; i < books.length; i++) {
                nextLine = bufferedReader.readLine();

                if (nextLine != null) {
                    String[] strings = nextLine.split(",");

                    String title = strings[0];
                    String author = strings[1];
                    String date = strings[2];

                    books[i] = new Book(title, author, date);
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println(e.getStackTrace());
            System.exit(1);
        }

        // Saves new books

        Book[] newBooks = new Book[3];
        newBooks[0] = new Book("The Hobbit", "J. R. R. Tolkien", "21 September 1937");
        newBooks[1] = new Book("Nineteen Eighty-Four", "George Orwell", "8 June 1949");
        newBooks[2] = new Book("Pride and Prejudice", "Jane Austen", "28 January 1813");

        try
        {
            FileWriter csvWriter = new FileWriter("data/allbooks.csv");

            csvWriter.append("Book Title");
            csvWriter.append(",");
            csvWriter.append("Author");
            csvWriter.append(",");
            csvWriter.append("Date");
            csvWriter.append("\n");

            for (int i = 0; i < newBooks.length; i++)
            {
                Book newBook = newBooks[i];

                csvWriter.append(newBooks[i].getTitle());
                csvWriter.append(",");
                csvWriter.append(newBooks[i].getAuthor());
                csvWriter.append(",");
                csvWriter.append(newBooks[i].getDate());
                csvWriter.append("\n");
            }

            csvWriter.flush();
            csvWriter.close();

        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            System.out.println(e.getStackTrace());
            System.exit(1);
        }
    }
}